# spectroscroller


Turn an image to a wav file, rendering the image in the spectrogram of the sound.

## Usage

spectroscroller.py [-s sample_rate (default 44100)] input_file output_file

## Image file

- Must be 320 pixels wide, 8bpp.
- Only red channel is used.

## Spectrogram

Be sure to set to linear scale.

## Dependencies
- numpy
- scipy.io
- PIL
- tqdm
