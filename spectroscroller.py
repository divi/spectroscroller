#!/usr/bin/env python

# Spectroscroller
# GPL V3
# (c) deevee 2022

import argparse
import sys
import math
import numpy as np
from scipy.io import wavfile
from PIL import Image
from tqdm import tqdm


class MainApp:
    def __init__(self, input_file, output_file, sample_rate):
        print("Converting {0} into {1} at {2} Hz...".format(input_file, output_file, sample_rate))

        self.pict = Image.open(input_file)
        self.pictWidth, self.pictHeight = self.pict.size

        if self.pictWidth != 320:
            print("Image width must be exactly 320 pixels.")
            sys.exit(1)

        self.oscillators = [Oscillator(i * 75 + 20, sample_rate) for i in range(320)]

        self.spect = iter(Spectrum(self.oscillators, self.pict))


        self.samples = [next(self.spect) for _ in tqdm(range(3250 * self.pict.size[1] - 1))]
#        self.samples = [next(self.spect) for _ in tqdm(range(1000))]

        self.wave_to_file(self.samples, output_file, 1, sample_rate)

        print("Done!")

    def wave_to_file(self, wav, fname, amp, sample_rate):
        wav = np.array(wav)
        wav = np.int16(wav * amp * (2 ** 15 - 1))

        wavfile.write(fname, sample_rate, wav)


class Spectrum:
    def __init__(self, oscillators, pict):
        self.oscillators = oscillators
        self.osc_count = len(self.oscillators)
        self.pict = pict
        self.samples_count = 0
        self.line = 0

    def __iter__(self):
        self.osc_it = [iter(osc) for osc in self.oscillators]
        return self

    def __next__(self):
        self.samples_count += 1
        if self.samples_count % 3250 == 0:
            self.line += 1

        sample = 0
        pix_id = 0
        for osc in self.osc_it:
            sample += next(osc) / self.osc_count * (self.pict.getpixel((pix_id, self.line))[0] / 255)
            pix_id += 1

        return sample


class Oscillator:
    def __init__(self, frequency, sample_rate):
        self.amplitude = 1
        self.increment = (2 * math.pi * frequency) / sample_rate
        self.step = 0

    def __iter__(self):
        return self

    def __next__(self):
        self.step += self.increment
        return math.sin(self.step)


if __name__ == "__main__":

    # Arguments handling
    arg_parser = argparse.ArgumentParser(prog="spectroscroller.py")
    arg_parser.add_argument("input", help="Input image file.")
    arg_parser.add_argument("output", help="Output audio file.")
    arg_parser.add_argument("-s", default="44100", help="Sample rate of audio file (default 44100 Hz).")
    args = arg_parser.parse_args()

    try:
        sample_rate = int(args.s)
    except ValueError:
        print("Invalid sample rate.")
        sys.exit(1)

    # Launch the app
    app = MainApp(args.input, args.output, sample_rate)

